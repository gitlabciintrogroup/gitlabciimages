#!/bin/sh

image=$1      #$FEDORA_BUILD   
startdir=$2   #docker-fedora
registry=$3   #$CI_REGISTRY
project=$4    #$PROJECT_GROUP

# docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
set -e
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $registry
docker build --no-cache -t $registry/$project:$image $startdir
docker push $registry/$project:$image

exit 0



